  Exo.FormView = Exo.View.extend({

    initialize: function (options) {
      this.group = options.defaultGroup || "*";
      this.schema = options.schema || {};
      this.setArg('schema', this.schema);
      this.setArg('group', this.group);
    },

    build: function () {
      _.each(this.schema.fields,
        function (field, fieldName) {
          if (field.groups &&
              field.groups.indexOf("*") !== -1 ||
              field.groups.indexOf(this.group) !== -1 &&
              !this.getChild(fieldName)) {
                 this.setChild(this.container.get(field.field, {
                   name: fieldName,
                   selector: '.' + fieldName,
                   schema: this.schema,
                   fieldName: fieldName,
                   model: this.model,
                   field: field,
                   visible: true
                 }));
          }
        }, this);
      return this;
    }

  });
