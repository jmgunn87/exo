  Exo.FieldView = Exo.View.extend({

    initialize: function (options) {
      this.fieldName = options.fieldName;
      this.field = options.field;
      this.schema = options.schema;
      this.setArg('name', this.fieldName);
      this.setArg('schema', this.schema);
      this.setArg('field', this.field);
    },

    serialize: function (cb) {
      var value = null;
      switch(this.field.type) {
        case 'choice':
          value = this.$el.find("." + this.fieldName).find("options:selected").val();
          break;
        default:
          value = this.$el.find("." + this.fieldName).val();
          break;
      }
      cb.call(this, value);
    }

  });

