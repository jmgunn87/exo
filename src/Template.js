  Exo.Template = function () {
    this._template = _.template(arguments[0] || "");
  };

  Exo.Template.prototype = {

    setTemplate: function (src) {
      this._template = _.template(src);
    },

    render: function (args) {
      return this._template({
        data: args, 
        lang: this.i18n
      });
    }

  };

