describe("Container", function () {

  beforeEach(function () {
    container = new Container();
    container.param('testkey', 'hello');
    container.service('testservice', function (p) {
      return new Object({ value: p['test'] || 100 });
    });
    container.factory('testfactory', function (p) {
      return new Object({ value: p['test'] || 100 });
    });
  });

  describe('#constructor', function () {
    it("initializes a new internal store", function () {
      expect(new Container().store).toEqual({});
    });
  });

  describe('#param', function () {
    it('sets a parameter on the container', function () {
      expect(container.store['testkey'].value).toEqual('hello');
    });
  });

  describe('#service', function () {
    it('defines a new service on the container', function () {
      expect(container.store['testservice'].constructor).toBeDefined();
    });
  });

  describe('#factory', function () {
    it('defines a new factory on the container', function () {
      expect(container.store['testfactory'].constructor).toBeDefined();
    });
  });

  describe('#get', function () {
    it("returns undefined when an id does not exist", function () {
      expect(container.get("null-key")).toBeUndefined();
    });
    it("returns the same parameter", function () {
      expect(container.get('testkey')).toEqual('hello');      
    });
    it("returns the same service", function () {
      var instance = container.get('testservice');
      expect(instance).toBeDefined();
      expect(instance.value).toBe(100);
      var instance2 = container.get('testservice', {test:1001});
      expect(instance2).toBeDefined();
      expect(instance2.value).not.toBe(1001);
    });
    it("return a new object from a factory", function () {
      var instance = container.get('testfactory');
      expect(instance).toBeDefined();
      expect(instance.value).toBe(100);
      var instance2 = container.get('testfactory', {test:1001});
      expect(instance2).toBeDefined();
      expect(instance2.value).toBe(1001);
    });
  });

  describe('#assemble', function () {
    it('constructs all services', function () {
      container.assemble(function () {
        expect(container.store["testservice"]).toBeDefined();
        expect(container.store["testservice"].value).toBeDefined();
      });
    });
  });

});
