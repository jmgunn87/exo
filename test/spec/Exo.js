describe("Exo", function () {
  it("has View constructor as a static property", function (){
    expect(Exo.View).toBeDefined();
  });
  it("has Template constructor as a static property", function (){
    expect(Exo.Template).toBeDefined();
  });
  it("has FormView constructor as a static property", function (){
    expect(Exo.FormView).toBeDefined();
  });
  it("has FieldView constructor as a static property", function (){
    expect(Exo.FieldView).toBeDefined();
  });
});
