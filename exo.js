;(function () { 

  "use strict"; 

 var Exo = {}; 


  Exo.View = function () {
    var options = arguments[0] || this;
    this.rendered = false;
    this.container = options.container;
    this.el = options.el;
    this.name = options.name || this.cid;
    this.args = options.args || {};
    this.template = options.template || undefined;
    this.selector = options.selector || "";
    this.visible = options.visible || false;
    this.parent = options.parent || undefined;
    this.children = options.children || {};
    if (this.modal) {
      this.events = this.events || {};
      this.events["click .modal-overlay"] = "hide";
      this.events["click .modal-overlay .modal"] = "_modal";
    }
    Backbone.View.prototype.constructor.call(this, options);
    if (this.parent !== undefined) {
      this.parent.setChild(this);
    }
  };

  Exo.View.prototype = _.extend(Backbone.View.prototype, {

    setArg: function (key, value) {
      this.args[key] = value;
    },
  
    getArg: function (key) {
      return this.args[key];
    },
  
    hasArg: function (key) {
      return key in this.args;
    },
  
    getArgs: function () {
      return this.args;
    },
  
    removeArg: function (key) {
      var arg = this.args[key];
      delete this.args[key];
      return arg;
    },
  
    removeArgs: function () {
      var args = this.args;
      this.args = {};
      return args;
    },
  
    setName: function (name) {
      this.name = name;
    },
  
    getName: function () {
      return this.name;
    },
  
    setChild: function (view) {
      this.children[view.name] = view;
      view.parent = this;
      if (this.rendered) {
        this.renderChild(view.name);
      }
      this.trigger('child', view);
      return view;
    },

    getChild: function (name) {
      return this.children[name];
    },

    getChildren: function () {
      return this.children;
    },

    hideChild: function (name) {
      var child = this.children[name];
      if (child) {
        child.visible = false;
        child.hide();
        child.trigger('hide');
      }
    },

    hideChildren: function () {
      var filter = arguments[0] || undefined;
      for (var name in this.children) {
        if (filter) {
          var passesFilter = false;
          for (var key in filter) {
            passesFilter = this.children[name][key] === filter[key];
          }
          if (passesFilter) {
            this.hideChild(name);
          }
        } else {
          this.hideChild(name);
        }
      }
    },

    showChild: function (name, exclusive) {
      var child = this.children[name];
      if (child) {
        if (exclusive) {
          this.hideChildren();
        }
        child.visible = true;
        child.show();
        child.trigger('show');
      }
    },

    showChildren: function () {
      var filter = arguments[0] || undefined;
      for (var name in this.children) {
        if (filter) {
          var passesFilter = false;
          for (var key in filter) {
            passesFilter = this.children[name][key] === filter[key];
          }
          if (passesFilter) {
            this.showChild(name);
          }
        } else {
         this.showChild(name);
        }
      }
    },

    removeChild: function (name) {
      var child = this.children[name];
      delete this.children[name];
      if (child) {
        child.remove();
        child.parent = undefined;
      }
      return child;
    },

    removeChildren: function () {
      var children = {};
      for (var name in this.children) {
        children[name] = this.removeChild(name);
      }
      this.children = {};
      return children;
    },

    renderChild: function (name) {
      var child = this.children[name];
      if (!child) return;
      var element = child.selector ?
        this.$el.find(child.selector) : this.$el;
      child.render();
      switch (child.order) {
        case 'prepend':
          element.prepend(child.$el);
          break;
        case 'append':
        case undefined:
          element.append(child.$el);
          break;
        default:
          var children = element.children();
          if (children.length >= child.order + 1) {
            $(children.get(child.order)).before(child.$el);
          } else {
            element.append(child.$el);
          }
      }
      if (child.visible === false) {
        child.hide();
      } else {
        child.show();
      }
    },

    renderChildren: function () {
      for (var name in this.children) {
        this.renderChild(name);
      }
    },

    render: function () {
      if (this.template) {
        var rendered = this.template.render(this.args);
        if (this.modal) {
          this.$el.html('<div class="modal-overlay"><div class="modal">' + rendered + '</div></div>');
        } else {
          this.$el.html(rendered);
        }
      }

      _.each([
        "draggable", 
        "droppable", 
        "resizable", 
        "selectable", 
        "sortable"
      ], function (ability) {
        if (ability in this) {
          console.log(ability);
          if (this.modal) {
            this.$el.find(".modal")[ability](this[ability]);
          } else {
            this.$el[ability](this[ability]);
          }
        }
      }, this);

      this.trigger('render');
      this.rendered = true;
      this.renderChildren();
    },
    
    show: function () {
      this.$el.show();
    },
    
    hide: function () {
      this.$el.hide();
    },

    _modal: function (e) {
      e.stopPropagation();
    }
    
  });

  Exo.View.extend = Backbone.View.extend;



  Exo.Template = function () {
    this._template = _.template(arguments[0] || "");
  };

  Exo.Template.prototype = {

    setTemplate: function (src) {
      this._template = _.template(src);
    },

    render: function (args) {
      return this._template({
        data: args, 
        lang: this.i18n
      });
    }

  };


  Exo.FormView = Exo.View.extend({

    initialize: function (options) {
      this.group = options.defaultGroup || "*";
      this.schema = options.schema || {};
      this.setArg('schema', this.schema);
      this.setArg('group', this.group);
    },

    build: function () {
      _.each(this.schema.fields,
        function (field, fieldName) {
          if (field.groups &&
              field.groups.indexOf("*") !== -1 ||
              field.groups.indexOf(this.group) !== -1 &&
              !this.getChild(fieldName)) {
                 this.setChild(this.container.get(field.field, {
                   name: fieldName,
                   selector: '.' + fieldName,
                   schema: this.schema,
                   fieldName: fieldName,
                   model: this.model,
                   field: field,
                   visible: true
                 }));
          }
        }, this);
      return this;
    }

  });

  Exo.FieldView = Exo.View.extend({

    initialize: function (options) {
      this.fieldName = options.fieldName;
      this.field = options.field;
      this.schema = options.schema;
      this.setArg('name', this.fieldName);
      this.setArg('schema', this.schema);
      this.setArg('field', this.field);
    }

  });



  window.Exo = Exo;

})();