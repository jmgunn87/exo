module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      files: {
        src: ['./src/**/*.js']
      }
    },
    concat: {
      options: {
        banner: ';(function () { \n\n  "use strict"; \n\n var Exo = {}; \n\n',
        seperator: "\n",
        footer: '\n\n  window.Exo = Exo;\n\n})();',
      },
      dev: {
        src: [
          'src/View.js', 
          'src/Template.js', 
          'src/FormView.js', 
          'src/FieldView.js'
        ],
        dest: './exo.js'
      }
    },
    jasmine: {
      all: {
        src: [
          'vendor/jquery/jquery.js',
          'vendor/underscore/underscore.js', 
          'vendor/backbone/backbone.js', 
          'exo.js'
        ],
        options: {
          specs: './test/spec/**/*.js'
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        report: 'gzip'
      },
      all: {
        files: {
          './exo.min.js': ['./exo.js']
        }
      }
    }
 });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('test', [
    'jshint',
    'concat',
    'jasmine'
  ]);
  grunt.registerTask('build', [
    'jshint',
    'concat',
    'jasmine',
    'uglify'
  ]);
};